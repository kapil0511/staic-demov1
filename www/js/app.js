angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'clients.controllers', 'login.controllers', 'statistics.controllers', 'galleries.controllers','galleriesdetails.controllers', 'principal.controllers', 'categories.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.Keyboard) {
      window.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $ionicConfigProvider.tabs.position('bottom');

  $stateProvider

  .state('login', {
        url: "/login",
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
  })

   .state('principal', {
        url: "/principal",
        templateUrl: 'templates/principal.html',
        controller: 'PrincipalCtrl'
  })

   .state('statistics', {
        url: "/statistics",
        templateUrl: 'templates/statistics.html',
        controller: 'StatisticsCtrl'
  })

    .state('categories', {
        url: "/categories",
        templateUrl: 'templates/categories.html',
        controller: 'CategoriesCtrl'
  })

     .state('galleries', {
        url: "/galleries",
        templateUrl: 'templates/galleries.html',
        controller: 'GalleriesCtrl'
  })

       .state('galleriesdetails', {
        url: "/galleriesdetails",
        templateUrl: 'templates/galleries-details.html',
        controller: 'GalleriesDetailsCtrl'
  })

        .state('clients', {
        url: "/clients",
        templateUrl: 'templates/clients.html',
        controller: 'ClientsCtrl'
  })



    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  $urlRouterProvider.otherwise('/login');

});
